<?php

declare(strict_types=1);

namespace App\Infrastructure\Fixture;

use App\Infrastructure\Doctrine\Repository\DictDepartmentRepository;
use App\Infrastructure\Doctrine\Repository\DictCurrencyRepository;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use App\Application\Cqrs\Command\CreateEmployeeCommand;

class EmployeeFixture {

    CONST TEST_EMPLOYEE_COUNT = 50;

    private DictDepartmentRepository $dictDepartmentRepository;
    private DictCurrencyRepository $dictCurrencyRepository;
    private MessageBusInterface $commandBus;
    private HttpClientInterface $client;

    function __construct(
            HttpClientInterface $client,
            MessageBusInterface $commandBus,
            DictDepartmentRepository $dictDepartmentRepository,
            DictCurrencyRepository $dictCurrencyRepository
    ) {
        $this->client = $client;
        $this->commandBus = $commandBus;

        $this->dictDepartmentRepository = $dictDepartmentRepository;
        $this->dictCurrencyRepository = $dictCurrencyRepository;
    }

    public function createFixtures(): bool {

        $faker = \Faker\Factory::create();

        $departments = $this->dictDepartmentRepository->findAll();

        $departmentsIds = [];
        foreach ($departments as $department) {
            $departmentsIds[] = $department;
        }

        $currencies = $this->dictCurrencyRepository->findAll();
        $currenciesIds = [];
        foreach ($currencies as $currency) {
            $currenciesIds[] = $currency;
        }

        for ($index = 0; $index < self::TEST_EMPLOYEE_COUNT; $index++) {

            $randomDepartments = array_rand($departmentsIds);
            $randomCurrencies = array_rand($currenciesIds);

            $command = new CreateEmployeeCommand();
            $command->setFirstName($faker->firstName());
            $command->setLastName($faker->lastName());
            $command->setDepartmentId($departmentsIds[$randomDepartments]);
            $command->setHiredAt($faker->dateTimeBetween('-20 years', 'now', 'Europe/Warsaw'));
            $command->setBasicSalary($faker->numberBetween(500, 11000));
            $command->setCurrencyId($currenciesIds[$randomCurrencies]);

            $this->commandBus->dispatch($command);
        }

        return true;
    }

}
