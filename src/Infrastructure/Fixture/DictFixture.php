<?php

declare(strict_types=1);

namespace App\Infrastructure\Fixture;

use App\Infrastructure\Service\PersistenceService;
use App\Domain\Model\DictCurrency;
use App\Domain\Model\DictDepartment;
use App\Domain\Model\PremiumConfig;
use App\Infrastructure\Doctrine\Type\PremiumTypeEnum;

class DictFixture {

    private PersistenceService $persistenceService;

    const DEPARTMENTS = [
        'hr' => [
            'description' => 'HR',
            'premiumType' => PremiumTypeEnum::TYPE_CONST_AMOUNT,
            'premiumValue' => 100
        ],
        'it' => [
            'description' => 'IT',
            'premiumType' => PremiumTypeEnum::TYPE_PERCENT_AMOUNT,
            'premiumValue' => 0.15,
            'currency' => null
        ],
        'customerService' => [
            'description' => 'Customer Service',
            'premiumType' => PremiumTypeEnum::TYPE_PERCENT_AMOUNT,
            'premiumValue' => 0.20,
            'currency' => null
        ]
    ];

    function __construct(
            PersistenceService $persistenceService
    ) {
        $this->persistenceService = $persistenceService;
    }

    public function createFixtures(): bool {

        $currency = new DictCurrency();
        $currency->setName('dollar');
        $currency->setDescription('American Dollar');
        $currency->setSymbol("$");

        $this->persistenceService->persist($currency);
        $this->persistenceService->flush();

        foreach (self::DEPARTMENTS as $key => $value) {

            $department = new DictDepartment();
            $department->setName($key);
            $department->setDescription($value["description"]);
            $this->persistenceService->persist($department);

            $premiumConfig = new PremiumConfig();
            $premiumConfig->setDictCurrency($currency);
            $premiumConfig->setDictDepartment($department);
            $premiumConfig->setType($value["premiumType"]);
            $premiumConfig->setValue($value["premiumValue"]);
            $currency = ($premiumConfig->getType() == PremiumTypeEnum::TYPE_CONST_AMOUNT) ? $currency : $value["currency"];

            $premiumConfig->setDictCurrency($currency);

            $this->persistenceService->persist($premiumConfig);
        }


        $this->persistenceService->flush();

        return true;
    }

}
