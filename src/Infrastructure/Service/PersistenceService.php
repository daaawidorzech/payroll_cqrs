<?php

declare(strict_types=1);

namespace App\Infrastructure\Service;

use App\Application\Exception\InvalidArgumentException;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class PersistenceService {

    private Connection $connection;
    private EntityManagerInterface $em;
    private ValidatorInterface $validator;

    public function __construct(
            EntityManagerInterface $em,
            Connection $connection,
            ValidatorInterface $validator
    ) {
        $this->em = $em;
        $this->connection = $connection;
        $this->validator = $validator;
    }

    public function getRepository(string $entityClass) {
        return $this->em->getRepository($entityClass);
    }

    public function beginTransaction() {
        $this->em->getConnection()->beginTransaction();
    }

    public function commit() {
        $this->em->getConnection()->commit();
    }

    public function rollback() {
        if ($this->em->getConnection()->isTransactionActive()) {
            $this->em->getConnection()->rollBack();
        }
    }

    public function save($entity) {
        if ($entity->getId()) {
            $this->merge($entity);
        } else {
            $this->persist($entity);
        }

        $this->flush();
    }

    public function remove($entity) {
        $this->em->remove($entity);
    }

    public function delete($entity) {
        $this->em->remove($entity);
        $this->flush();
    }

    public function merge($entity) {
        $this->validate($entity);
        $this->em->merge($entity);
    }

    public function persist($entity) {
        $this->validate($entity);
        $this->em->persist($entity);
    }

    public function flush() {
        $this->em->flush();
    }

    private function validate($entity) {

        $errorsResponse = '';
        $violations = $this->validator->validate($entity);
        if ($violations->count() > 0) {
            foreach ($violations as $key => $error) {
                $errorsResponse .= $error->getMessage() . ".";
            }

            throw new InvalidArgumentException($errorsResponse);
        }
    }

}
