<?php

declare(strict_types=1);

namespace App\Infrastructure\SpreadSheet;

use App\Infrastructure\SpreadSheet\Factory\FileFactory;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class SpreadSheetCreator {

    private ParameterBagInterface $params;
    private string $tmpFolder;
    private FileFactory $fileFactory;

    function __construct(
            ParameterBagInterface $params,
            FileFactory $fileFactory
    ) {
        $this->params = $params;
        $this->tmpFolder = $this->params->get('files')['tmpFilePath'];
        $this->fileFactory = $fileFactory;
    }

    public function createSpreadSheet(array $data, string $type) {

        return $this->fileFactory->createFrom($data, $this->tmpFolder, $type)->createFile();
    }

}
