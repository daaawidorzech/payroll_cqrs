<?php

declare(strict_types=1);

namespace App\Infrastructure\SpreadSheet\Factory;

use App\Application\Exception\InvalidArgumentException;
use App\Infrastructure\SpreadSheet\Model\Xls;
use App\Infrastructure\SpreadSheet\Model\Csv;

class FileFactory {

    const TYPE_XLS = 'xls';
    const TYPE_CSV = 'csv';

    public function createFrom(array $data, string $fileSavePath, string $type) {

        switch ($type) {
            case self::TYPE_XLS:

                return new Xls($data, $fileSavePath);
            case self::TYPE_CSV:

                return new Csv($data, $fileSavePath);
            default:
                throw new InvalidArgumentException("Invalid response type");
        }
    }

}
