<?php

declare(strict_types=1);

namespace App\Infrastructure\SpreadSheet\Model;

use App\Infrastructure\SpreadSheet\Model\FileInterface;
use App\Infrastructure\SpreadSheet\Model\AbstractFile;
use \PhpOffice\PhpSpreadsheet\Spreadsheet;

class Xls extends AbstractFile implements FileInterface {

    protected string $extension = '.xls';
    protected array $data;
    protected string $fullPath;
    protected string $fileSavePath;

    function __construct(
            $data,
            string $fileSavePath
    ) {
        $this->data = $data;
        $this->fileSavePath = $fileSavePath;
    }

    public function createFile() {

        $spreadsheet = new Spreadsheet();
        $spreadsheet->getActiveSheet()
                ->fromArray(
                        array_merge($this->columns, $this->data),
                        NULL,
                        'A1',
                        true
        );

        $this->setFileName($this->fileName . date("_Ymd_His") . $this->extension);
        $this->setFullPath($this->fileSavePath . $this->fileName);

        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xls($spreadsheet);
        $writer->save($this->getFullPath());

        return $this;
    }

    function setData(array $data): void {
        $this->data = $data;
    }

    function getFullPath(): string {
        return $this->fullPath;
    }

    function setFullPath(string $fullPath): void {
        $this->fullPath = $fullPath;
    }

}
