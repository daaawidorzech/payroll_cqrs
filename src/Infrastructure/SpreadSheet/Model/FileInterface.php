<?php

declare(strict_types=1);

namespace App\Infrastructure\SpreadSheet\Model;

interface FileInterface {

    public function setData(array $data);
}
