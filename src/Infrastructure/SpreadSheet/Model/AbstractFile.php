<?php

declare(strict_types=1);

namespace App\Infrastructure\SpreadSheet\Model;

abstract class AbstractFile {

    protected array $columns = [array(
    'Imię', 'Nazwisko', 'Dział', 'Podstawa Wynagrodzenia (kwota)', 'Dodatek do podstawy (kwota)', 'Typ dodatku (typ % lub stały)', 'Wynagrodzenie wraz z dodatkiem (kwota)'
    )];
    protected string $fileName = 'raport';
    protected string $extension;
    protected string $fileSavePath;
    protected string $fullPath;

    function getFileName(): string {
        return $this->fileName;
    }

    function setFullPath(string $fullPath): void {
        $this->fullPath = $fullPath;
    }

    function setFileName(string $fileName): void {
        $this->fileName = $fileName;
    }

}
