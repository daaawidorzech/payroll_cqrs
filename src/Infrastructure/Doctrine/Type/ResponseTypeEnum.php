<?php

declare(strict_types=1);

namespace App\Infrastructure\Doctrine\Type;

use App\Exception\InvalidArgumentException;

class ResponseTypeEnum {

    const TYPE_RESPONSE_JSON = 'json';
    const TYPE_RESPONSE_CSV = 'csv';
    const TYPE_RESPONSE_XLSX = 'xlsx';

    protected $name = 'enumResponseTypes';
    protected static $values = [
        self::TYPE_RESPONSE_JSON => self::TYPE_RESPONSE_JSON,
        self::TYPE_RESPONSE_CSV => self::TYPE_RESPONSE_CSV,
        self::TYPE_RESPONSE_XLSX => self::TYPE_RESPONSE_XLSX
    ];

    public static function getValue(string $typeString): string {
        if (!isset(self::$values[$typeString])) {
            throw new InvalidArgumentException(\sprintf('Invalid premium type: %s', $typeString));
        }
        return self::$values[$type];
    }

    public static function getTypes() {
        return $this->values;
    }

    public static function fromValue(string $findType): string {

        foreach (self::$values as $key => $type) {
            if ($type == $findType) {
                return $key;
            }
        }
        throw new InvalidArgumentException(\sprintf('Invalid premium type: %s', $findType));
    }

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform) {
        $values = array_map(function($val) {
            return "'" . $val . "'";
        }, $this->values);

        return "ENUM(" . implode(", ", $values) . ")";
    }

    public function convertToPHPValue($value, AbstractPlatform $platform) {
        return $value;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform) {
        if (!in_array($value, $this->values)) {
            throw new InvalidArgumentException("Invalid '" . $this->name . "' value.");
        }
        return $value;
    }

    public function getName() {
        return $this->name;
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform) {
        return true;
    }

}
