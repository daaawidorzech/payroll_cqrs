<?php

declare(strict_types=1);

namespace App\Infrastructure\Doctrine\Type;

use App\Application\Exception\InvalidArgumentException;
use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;

class PremiumTypeEnum extends Type {

    const TYPE_CONST_AMOUNT = 'CONST_AMOUNT';
    const TYPE_PERCENT_AMOUNT = 'PERCENT_AMOUNT';

    protected $name = 'enumPremiumTypes';
    protected static $values = [
        self::TYPE_CONST_AMOUNT => self::TYPE_CONST_AMOUNT,
        self::TYPE_PERCENT_AMOUNT => self::TYPE_PERCENT_AMOUNT
    ];

    public static function getValue(string $typeString): string {
        if (!isset(self::$values[$typeString])) {
            throw new InvalidArgumentException(\sprintf('Invalid premium type: %s', $typeString));
        }
        return self::$values[$type];
    }

    public static function getTypes() {
        return self::$values;
    }

    public static function fromValue(string $findType): string {

        foreach (self::$values as $key => $type) {
            if ($type == $findType) {
                return $key;
            }
        }
        throw new InvalidArgumentException(\sprintf('Invalid premium type: %s', $findType));
    }

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform) {
        $values = array_map(function($val) {
            return "'" . $val . "'";
        }, $this->values);

        return "ENUM(" . implode(", ", $values) . ")";
    }

    public function convertToPHPValue($value, AbstractPlatform $platform) {
        return $value;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform) {
        if (!in_array($value, $this->values)) {
            throw new InvalidArgumentException("Invalid '" . $this->name . "' value.");
        }
        return $value;
    }

    public function getName() {
        return $this->name;
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform) {
        return true;
    }

}
