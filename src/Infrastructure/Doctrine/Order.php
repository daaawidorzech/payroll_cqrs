<?php

declare(strict_types=1);

namespace App\Infrastructure\Doctrine;

class Order {

    private string $column;
    private ?string $direction;

    function __construct(string $column, ?string $direction) {
        $this->column = $column;
        $this->direction = $direction;
    }

    function getColumn(): string {
        return $this->column;
    }

    function getDirection(): ?string {
        return $this->direction;
    }

    function setColumn(string $column): void {
        $this->column = $column;
    }

    function setDirection(?string $direction): void {
        $this->direction = $direction;
    }

}
