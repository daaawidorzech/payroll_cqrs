<?php

declare(strict_types=1);

namespace App\Infrastructure\Doctrine;

class Filter {

    const FILTER_RIGHT_LIKE = 'rightLike';
    const FILTER_EQ = 'eq';
    const FILTER_GTE = 'gte';
    const FILTER_LTE = 'lte';
    const FILTER_CONTAINS = 'contains';
    const FILTER_IS_EMPTY = 'isEmpty';
    const FILTER_BETWEEN = 'between';

    private string $field;

    /** @var mixed */
    private $value;
    private string $operator;

    public function __construct(string $field, string $operator, $value) {
        $this->field = $field;
        $this->operator = $operator;
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getField(): string {
        return $this->field;
    }

    /**
     * @return mixed
     */
    public function getValue() {
        return $this->value;
    }

    /**
     * @return string
     */
    public function getOperator(): string {
        return $this->operator;
    }

}
