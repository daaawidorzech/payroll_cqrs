<?php

declare(strict_types=1);

namespace App\Infrastructure\Doctrine;

use Doctrine\Common\Collections\ArrayCollection;

class FilterContainer {

    /** @var Filter[]|ArrayCollection */
    private ArrayCollection $filters;
    
    /** @var Filter[]|ArrayCollection */
    private ArrayCollection $order;

    public function __construct() {
        $this->filters = new ArrayCollection();
        $this->order = new ArrayCollection();
    }

    public function addFilter(Filter $filter): void {
        $this->filters->add($filter);
    }

    public function createFilter(string $field, string $operator, $value): void {
        $this->filters->add(new Filter($field, $operator, $value));
    }

    public function getFilters(): ArrayCollection {
        return $this->filters;
    }

    public function addOrder(Filter $filter): void {
        $this->order->add($filter);
    }

    public function createOrder(?string $column, ?string $direction): void {
        if ($column && $direction) {
            $this->order->add(new Order($column, $direction));
        }
    }

    public function getOrder(): ArrayCollection {
        return $this->order;
    }

}
