<?php

declare(strict_types=1);

namespace App\Infrastructure\Doctrine\Repository;

use App\Domain\Model\EmployeeSalary;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EmployeeSalary|null find($id, $lockMode = null, $lockVersion = null)
 * @method EmployeeSalary|null findOneBy(array $criteria, array $orderBy = null)
 * @method EmployeeSalary[]    findAll()
 * @method EmployeeSalary[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EmployeeSalaryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EmployeeSalary::class);
    }
}
