<?php

declare(strict_types=1);

namespace App\Infrastructure\Doctrine\Repository;

use App\Domain\Model\PremiumConfig;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PremiumConfig|null find($id, $lockMode = null, $lockVersion = null)
 * @method PremiumConfig|null findOneBy(array $criteria, array $orderBy = null)
 * @method PremiumConfig[]    findAll()
 * @method PremiumConfig[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PremiumConfigRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PremiumConfig::class);
    }
}
