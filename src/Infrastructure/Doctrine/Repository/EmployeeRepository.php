<?php

declare(strict_types=1);

namespace App\Infrastructure\Doctrine\Repository;

use App\Domain\Model\Employee;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Infrastructure\Doctrine\FilterContainer;
use App\Infrastructure\Doctrine\FilterContainerParser;

/**
 * @method Employee|null find($id, $lockMode = null, $lockVersion = null)
 * @method Employee|null findOneBy(array $criteria, array $orderBy = null)
 * @method Employee[]    findAll()
 * @method Employee[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EmployeeRepository extends ServiceEntityRepository {

    private FilterContainerParser $filterContainerParser;

    public function __construct(ManagerRegistry $registry, FilterContainerParser $filterContainerParser) {
        parent::__construct($registry, Employee::class);
        $this->filterContainerParser = $filterContainerParser;
    }

    public function findByFilters(FilterContainer $filterContainer = null) {

        $qb = $this->createQueryBuilder('e');
        $qb->select('e.firstName as firstName', 'e.lastName as lastName', 'd.description as departmentName', 'es.basicSalary as basicSalary', 'es.premium as premium', 'pc.type as premiumType', 'es.salary as salary');
        $qb->join('e.employee', 'es');
        $qb->join('e.dictDepartment', 'd');
        $qb->join('d.premiumConfig', 'pc');
        $qb->join('es.dictCurrency', 'dc');

        if ($filterContainer) {
            $this->filterContainerParser->filter($qb, $filterContainer);
        }
    
        return $qb->getQuery()->getResult();
    }

}
