<?php

declare(strict_types=1);

namespace App\Infrastructure\Doctrine;

use App\Infrastructure\Exception\UnknownFilterException;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class FilterContainerParser {

    public function filter(QueryBuilder $qb, FilterContainer $filterContainer): void {
        /** @var Filter $filter */
        foreach ($filterContainer->getFilters() as $filter) {
            switch ($filter->getOperator()) {
                case Filter::FILTER_IS_EMPTY:
                    $qb
                            ->andWhere($qb->expr()->orX(
                                            $qb->expr()->isNull($filter->getField()),
                                            $qb->expr()->eq($filter->getField(), ':emptyString')
                    ));

                    $qb->setParameter('emptyString', '');
                    break;
                case Filter::FILTER_BETWEEN:
                    $fieldMd5Gte = 'paramGte' . md5($filter->getField());
                    $fieldMd5Lte = 'paramLte' . md5($filter->getField());

                    $values = explode(';', $filter->getValue());

                    if (count($values) !== 2) {
                        throw new BadRequestHttpException('Filter between should be separated like "gteValue;lteValue"');
                    }

                    $qb->andWhere($qb->expr()->andX(
                                    $qb->expr()->gte($filter->getField(), (':' . $fieldMd5Gte)),
                                    $qb->expr()->lte($filter->getField(), (':' . $fieldMd5Lte))
                    ));

                    $qb->setParameter($fieldMd5Gte, $values[0]);
                    $qb->setParameter($fieldMd5Lte, $values[1]);
                    break;
                default:
                    $fieldMd5 = 'param' . md5($filter->getField());
                    $funcName = $this->getFilterFunctionName($filter->getOperator());
                    $qb->andWhere($qb->expr()->$funcName(
                                    $filter->getField(),
                                    (':' . $fieldMd5)
                    ));

                    $qb->setParameter($fieldMd5, $this->parseFilterValue($filter->getOperator(), $filter->getValue()));
                    break;
            }
        }

        foreach ($filterContainer->getOrder() as $order) {
            
            $qb->orderBy($order->getColumn(), $order->getDirection());
        }
    }

    private function getFilterFunctionName(string $operator): ?string {
        switch ($operator) {
            case Filter::FILTER_CONTAINS:
            case Filter::FILTER_RIGHT_LIKE:
                return 'like';
            case Filter::FILTER_EQ:
                return 'eq';
            case Filter::FILTER_LTE:
                return 'lte';
            case Filter::FILTER_GTE:
                return 'gte';
            case Filter::FILTER_BETWEEN:
            case Filter::FILTER_IS_EMPTY:
                return null;
        }

        throw new UnknownFilterException();
    }

    private function parseFilterValue(string $operator, $value) {
        switch ($operator) {
            case Filter::FILTER_RIGHT_LIKE:
                return $value . '%';
            case Filter::FILTER_CONTAINS:
                return '%' . $value . '%';
            default:
                return $value;
        }
    }

}
