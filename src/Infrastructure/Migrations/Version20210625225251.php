<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210625225251 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE premium_config ADD dict_department_id INT NOT NULL');
        $this->addSql('ALTER TABLE premium_config ADD CONSTRAINT FK_A630C8A1D5E94944 FOREIGN KEY (dict_department_id) REFERENCES dict_department (id)');
        $this->addSql('CREATE INDEX IDX_A630C8A1D5E94944 ON premium_config (dict_department_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE premium_config DROP FOREIGN KEY FK_A630C8A1D5E94944');
        $this->addSql('DROP INDEX IDX_A630C8A1D5E94944 ON premium_config');
        $this->addSql('ALTER TABLE premium_config DROP dict_department_id');
    }
}
