<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210627201827 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E3CEACF95E237E06 ON dict_currency (name)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6CBEFF0B5E237E06 ON dict_department (name)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX UNIQ_E3CEACF95E237E06 ON dict_currency');
        $this->addSql('DROP INDEX UNIQ_6CBEFF0B5E237E06 ON dict_department');
    }
}
