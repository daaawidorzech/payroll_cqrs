<?php

declare(strict_types=1);

namespace App\Application\Form;

use App\Application\Cqrs\Command\CreateEmployeeCommand;
use App\Domain\Model\DictCurrency;
use App\Domain\Model\DictDepartment;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class CreateEmployeeForm extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
                ->add('firstName', TextType::class)
                ->add('lastName', TextType::class)
                ->add('currencyId', EntityType::class, [
                    'class' => DictCurrency::class,
                    'invalid_message' => 'Invalid currencyId',
                ])
                ->add('departmentId', EntityType::class, [
                    'class' => DictDepartment::class,
                    'invalid_message' => 'Invalid departmentId',
                ])
                ->add('hiredAt', DateType::class, [
                    'widget' => 'single_text',
                    // this is actually the default format for single_text
                    'format' => 'yyyy-MM-dd',
                ])
                ->add('basicSalary', TextType::class)

        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        
        $resolver->setDefaults([
            'data_class' => CreateEmployeeCommand::class,
            'csrf_protection' => false,
            'allow_extra_fields' => true,
        ]);
    }

}
