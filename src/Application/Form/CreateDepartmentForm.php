<?php

declare(strict_types=1);

namespace App\Application\Form;

use App\Application\Cqrs\Command\CreateDepartmentCommand;
use App\Domain\Model\DictCurrency;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CreateDepartmentForm extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
                ->add('name', TextType::class)
                ->add('description', TextType::class)
                ->add('currencyId', EntityType::class, [
                    'class' => DictCurrency::class,
                    'invalid_message' => 'Invalid currencyId',
                ])
                ->add('premiumType', TextType::class)
                ->add('premiumValue', TextType::class)

        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        
        $resolver->setDefaults([
            'data_class' => CreateDepartmentCommand::class,
            'csrf_protection' => false,
            'allow_extra_fields' => true,
        ]);
    }

}
