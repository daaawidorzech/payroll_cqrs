<?php

declare(strict_types=1);

namespace App\Application\Form\Helper;

use Symfony\Component\Form\FormInterface;

class ErrorParser {

    public static function getArray(FormInterface $form) {

        return self::getErrors($form);
    }

    public static function getArrayValues(array $data): array {

        $result = [];

        foreach ($data as $key => $value) {
            if (is_array($value)) {
                foreach (self::getArrayValues($value) as $child) {
                    $result[] = $child;
                }
            } else {
                $result[] = $value;
            }
        }

        return $result;
    }

    public static function getString(FormInterface $form): string {

        $errors = self::getErrors($form);

        $string = "";

        foreach ($errors as $error) {
            if (is_array($error)) {
                $string .= implode(". ", self::getArrayValues([$error])) . ". ";
            } else {
                $string .= $error . ". ";
            }
        }

        return $string;
    }

    public static function getErrors(FormInterface $form): array {

        $errors = array();

        $i = 0;
        foreach ($form->getErrors() as $error) {
            $errors[$i] = $error->getMessage();
            $i++;
        }

        foreach ($form->all() as $key => $child) {
            /** @var $child FormInterface */
            if ($err = self::getErrors($child)) {
                $errors[$key] = $err;
            }
        }

        return $errors;
    }

}
