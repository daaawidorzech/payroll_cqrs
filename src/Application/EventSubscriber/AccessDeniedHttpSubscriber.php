<?php

declare(strict_types=1);

namespace App\Application\EventSubscriber;

use App\Application\Response\ErrorResponse;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\KernelEvents;

class AccessDeniedHttpSubscriber implements EventSubscriberInterface {

    public static function getSubscribedEvents() {
        
        return [
            KernelEvents::EXCEPTION => 'onAccessDeniedHttpException'
        ];
    }

    public function onAccessDeniedHttpException(ExceptionEvent $event) {
        
        if ($event->getThrowable() instanceof AccessDeniedHttpException) {
            $response = new ErrorResponse('Brak dostępu', [], Response::HTTP_FORBIDDEN);
            $event->setResponse($response);
        }
    }

}
