<?php

declare(strict_types=1);

namespace App\Application\EventSubscriber;

use App\Application\Response\ErrorResponse;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\KernelEvents;

class NotFoundHttpSubscriber implements EventSubscriberInterface {

    public static function getSubscribedEvents() {

        return [
            KernelEvents::EXCEPTION => 'onNotFoundHttpException'
        ];
    }

    public function onNotFoundHttpException(ExceptionEvent $event) {

        if ($event->getThrowable() instanceof NotFoundHttpException) {
            $response = new ErrorResponse('Nie znaleziono wskazanego obiektu', [], Response::HTTP_NOT_FOUND);
            $event->setResponse($response);
        }
    }

}
