<?php

declare(strict_types=1);

namespace App\Application\EventSubscriber;

use App\Application\Response\ErrorResponse;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\KernelEvents;

class BadRequestHttpSubscriber implements EventSubscriberInterface {

    public static function getSubscribedEvents() {
        
        return [
            KernelEvents::EXCEPTION => 'onBadRequestHttpException'
        ];
    }

    public function onBadRequestHttpException(ExceptionEvent $event) {
        
        $e = $event->getThrowable();
        if ($event->getThrowable() instanceof BadRequestHttpException) {
            $response = new ErrorResponse($e->getMessage(), [], Response::HTTP_BAD_REQUEST);
            $event->setResponse($response);
        }
    }

}
