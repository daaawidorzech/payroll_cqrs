<?php

declare(strict_types=1);

namespace App\Application\EventSubscriber;

use App\Application\Response\ErrorResponse;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\HttpKernel\KernelEvents;

class HandlerFailerHttpSubscriber implements EventSubscriberInterface {

    public static function getSubscribedEvents() {

        return [
            KernelEvents::EXCEPTION => 'onHandlerFailedException'
        ];
    }

    public function onHandlerFailedException(ExceptionEvent $event) {

        $e = $event->getThrowable();
        if ($event->getThrowable() instanceof HandlerFailedException) {
            $prevoius = $e->getPrevious();

            $response = new ErrorResponse($prevoius->getMessage(), [], Response::HTTP_BAD_REQUEST);
            $event->setResponse($response);
        }
    }

}
