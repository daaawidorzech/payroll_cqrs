<?php

declare(strict_types=1);

namespace App\Application\Response\Factory;

use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Response;

class ListResponseFactory {

    private SerializerInterface $serializer;

    public function __construct(SerializerInterface $serializer) {
        $this->serializer = $serializer;
    }

    public function create(array $results): Response {

        $responseData = [
            'items' => $results,
            'total' => count($results)
        ];

        $response = new Response();
        $response->setContent($this->serializer->serialize($responseData, 'json'));
        $response->headers->set('Content-type', 'application/json');
        $response->setStatusCode(Response::HTTP_OK);

        return $response;
    }

}
