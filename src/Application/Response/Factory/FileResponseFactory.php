<?php

declare(strict_types=1);

namespace App\Application\Response\Factory;

use App\Application\Cqrs\FileResult;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\File\Stream;

class FileResponseFactory {

    public function create(string $fileName, string $filePath): Response {

        $file = new Stream($filePath);
        $response = new BinaryFileResponse($file);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $fileName);
        $response->deleteFileAfterSend(true);


        return $response;
    }

    public function createFromFileResult(FileResult $fileResult): Response {
        
        return $this->create(
                        $fileResult->getFilename(),
                        $fileResult->getMime(),
                        $fileResult->getContent()
        );
    }

}
