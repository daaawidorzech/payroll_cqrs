<?php

declare(strict_types=1);

namespace App\Application\Response;

use App\Application\Form\Helper\ErrorParser;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class ErrorResponse extends JsonResponse {

    public function __construct(string $message, array $errors = [], int $httpCode = 500) {

        parent::__construct([
            'error' => [
                'errorId' => $httpCode,
                'errorText' => $message,
                'errorArray' => $errors,
            ]
                ], $httpCode);
    }

    public static function createFromForm(FormInterface $form) {

        return new self(ErrorParser::getString($form), ErrorParser::getArray($form), Response::HTTP_BAD_REQUEST);
    }

}
