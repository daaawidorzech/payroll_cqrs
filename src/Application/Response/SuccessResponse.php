<?php

declare(strict_types=1);

namespace App\Application\Response;

use Symfony\Component\HttpFoundation\JsonResponse;

class SuccessResponse extends JsonResponse {

    public function __construct(int $status = 200, array $headers = [], bool $json = false) {

        parent::__construct([
            'success' => true
                ], $status, $headers, $json);
    }

}
