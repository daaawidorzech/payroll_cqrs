<?php

declare(strict_types=1);

namespace App\Application\Controller\Employee;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Messenger\MessageBusInterface;
use App\Application\Common\ControllerHelper;
use App\Application\Cqrs\Command\CreateEmployeeCommand;
use Symfony\Component\Routing\Annotation\Route;
use App\Application\Form\CreateEmployeeForm;
use App\Application\Response\ErrorResponse;
use App\Application\Response\SuccessResponse;

class CreateEmployeeController extends AbstractController {

    private MessageBusInterface $commandBus;
    private ControllerHelper $controllerHelper;

    public function __construct(
            MessageBusInterface $commandBus,
            ControllerHelper $controllerHelper
    ) {
        $this->commandBus = $commandBus;
        $this->controllerHelper = $controllerHelper;
    }

    /**
     * Create employee
     * @Route("/employee", methods={"POST"})
     */
    public function createDepartment() {

        $command = new CreateEmployeeCommand();
        $form = $this->createForm(CreateEmployeeForm::class, $command);
        $form->submit($this->controllerHelper->getRequestData());

        if (!$form->isValid()) {
            return ErrorResponse::createFromForm($form);
        }

        $this->commandBus->dispatch($command);

        return new SuccessResponse();
    }

}
