<?php

declare(strict_types=1);

namespace App\Application\Controller\Employee;

use Symfony\Component\Routing\Annotation\Route;
use App\Application\Cqrs\Query\EmployeeList;
use App\Application\Cqrs\QueryParams\EmployeeListParams;
use App\Application\Response\Factory\ListResponseFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;
use App\Application\Response\Factory\FileResponseFactory;
use App\Infrastructure\SpreadSheet\SpreadSheetCreator;
use App\Application\Cqrs\QueryParams\ResponseTypeParams;

class GetEmployeeListController extends AbstractController {

    private EmployeeList $employeeList;
    private ListResponseFactory $listResponseFactory;
    private RequestStack $requestStack;
    private FileResponseFactory $fileResponseFactory;
    private SpreadSheetCreator $spreadSheetCreator;

    function __construct(
            EmployeeList $employeeList,
            ListResponseFactory $listResponseFactory,
            RequestStack $requestStack,
            FileResponseFactory $fileResponseFactory,
            SpreadSheetCreator $spreadSheetCreator
    ) {
        $this->employeeList = $employeeList;
        $this->listResponseFactory = $listResponseFactory;
        $this->requestStack = $requestStack;
        $this->fileResponseFactory = $fileResponseFactory;
        $this->spreadSheetCreator = $spreadSheetCreator;
    }

    /**
     * Create payroll export report
     * @Route("/report", methods={"GET"})
     */
    public function getReport() {

        $listParams = new EmployeeListParams($this->requestStack->getCurrentRequest());
        $responseTypeParams = new ResponseTypeParams($this->requestStack->getCurrentRequest());

        $results = $this->employeeList->query($listParams);
        if ($responseTypeParams->getResponseType() == $responseTypeParams::RESPONSE_TYPE_JSON) {
            return $this->listResponseFactory->create($results);
        }

        $file = $this->spreadSheetCreator->createSpreadSheet($results, $responseTypeParams->getResponseType());

        return $this->fileResponseFactory->create($file->getFileName(), $file->getFullPath());
    }

}
