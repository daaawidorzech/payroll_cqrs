<?php

declare(strict_types=1);

namespace App\Application\Controller\Department;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Messenger\MessageBusInterface;
use App\Application\Common\ControllerHelper;
use App\Application\Cqrs\Command\CreateDepartmentCommand;
use Symfony\Component\Routing\Annotation\Route;
use App\Application\Form\CreateDepartmentForm;
use App\Application\Response\ErrorResponse;
use App\Application\Response\SuccessResponse;

class CreateDepartmentController extends AbstractController {

    private MessageBusInterface $commandBus;
    private ControllerHelper $controllerHelper;

    public function __construct(
            MessageBusInterface $commandBus,
            ControllerHelper $controllerHelper
    ) {
        $this->commandBus = $commandBus;
        $this->controllerHelper = $controllerHelper;
    }

    /**
     * Create employee
     * @Route("/department", methods={"POST"})
     */
    public function createDepartment() {

        $command = new CreateDepartmentCommand();
        $form = $this->createForm(CreateDepartmentForm::class, $command);
        $form->submit($this->controllerHelper->getRequestData());

        if (!$form->isValid()) {
            return ErrorResponse::createFromForm($form);
        }

        $this->commandBus->dispatch($command);

        return new SuccessResponse();
    }

}
