<?php

declare(strict_types=1);

namespace App\Application\Exception;

use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException as Symfony403;

class AccessDeniedException extends Symfony403 {
    
}
