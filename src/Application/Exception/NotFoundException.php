<?php

declare(strict_types=1);

namespace App\Application\Exception;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException as Symfony404;

class NotFoundException extends Symfony404 {

    public function __construct(string $message = null, \Exception $previous = null, int $code = 0, array $headers = []) {
        parent::__construct($message, $previous, 404, $headers);
    }

}
