<?php

declare(strict_types=1);

namespace App\Application\Common;

use Symfony\Component\HttpFoundation\RequestStack;

class ControllerHelper {

    protected RequestStack $requestStack;

    public function __construct(
            RequestStack $requestStack
    ) {
        $this->requestStack = $requestStack;
    }

    public function getRequestData(): array {

        return json_decode($this->requestStack->getCurrentRequest()->getContent(), true);
    }

}
