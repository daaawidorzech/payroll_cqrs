<?php

declare(strict_types=1);

namespace App\Application\Cqrs\QueryParams;

use Symfony\Component\HttpFoundation\Request;

class EmployeeListParams extends AbstractParams {

    protected ?string $firstName;
    protected ?string $lastName;
    protected ?string $departmentName;

    public function getParamTypes(): array {
        return [
            'firstName' => 'string',
            'lastName' => 'string',
            'departmentName' => 'string',
        ];
    }

    public function getAliasses(): array {
        return [
            'firstName' => 'e',
            'lastName' => 'e',
            'departmentName' => 'd',
        ];
    }

    public function mapDbName(): array {
        return [
            'departmentName' => 'description',
        ];
    }

    public function getOrderColumns(): array {
        return[
            'firstName', 'lastName', 'departmentName', 'basicSalary', 'premium', 'premiumType', 'salary'
        ];
    }

    public function __construct(Request $request) {
        $this->parseFilters($request);
    }

    public function getDefaultAlias(): string {
        return 'e';
    }

    function getFirstName(): ?string {
        return $this->firstName;
    }

    function getLastName(): ?string {
        return $this->lastName;
    }

    function getDepartmentName(): ?string {
        return $this->departmentName;
    }

    function setFirstName(?string $firstName): void {
        $this->firstName = $firstName;
    }

    function setLastName(?string $lastName): void {
        $this->lastName = $lastName;
    }

    function setDepartmentName(?string $departmentName): void {
        $this->departmentName = $departmentName;
    }

}
