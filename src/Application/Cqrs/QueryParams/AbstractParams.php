<?php

declare(strict_types=1);

namespace App\Application\Cqrs\QueryParams;

use App\Infrastructure\Doctrine\Filter;
use App\Infrastructure\Doctrine\FilterContainer;
use ReflectionClass;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractParams {

    abstract public function getAliasses(): array;

    abstract public function getParamTypes(): array;

    abstract public function getOrderColumns(): array;

    protected ?string $orderColumn = null;
    protected string $orderDirection = 'asc';
    private array $__operators;

    function getOrderColumn() {
        return $this->orderColumn;
    }

    function getOrderDirection() {
        return $this->orderDirection;
    }

    function setOrderColumn($orderColumn): void {
        $this->orderColumn = $orderColumn;
    }

    function setOrderDirection($orderDirection): void {
        $this->orderDirection = $orderDirection;
    }

    public function parseFilters(Request $request): void {

        $this->__operators = [];
        $reflectionClass = new ReflectionClass(get_class($this));

        $fields = $reflectionClass->getProperties();
        $filters = $request->query->all()['filter'] ?? [];
        $orders = $request->query->all()['order'] ?? [];
       
        if (!is_array($filters)) {
            $filters = [];
        }

        $filtersWithoutOperator = [];
        foreach ($filters as $key => $value) {
            $parts = explode(':', $key);
            if (isset($parts[1])) {
                $filtersWithoutOperator[$parts[1]] = $value;

                $this->__operators[$parts[1]] = $parts[0];
            } else {
                $filtersWithoutOperator[$key] = $value;
            }
        }

        foreach ($fields as $field) {

            if ($this->isOrderParam($field)) {
 
                if ($this->isAvailableOrder($orders)) {
                   
                    $this->orderColumn = (isset($orders['column'])) ? $orders['column'] : $this->orderColumn;
                    $this->orderDirection = (isset($orders['direction'])) ? $orders['direction'] : $this->orderDirection;
                }
                continue;
            }

            $value = isset($filtersWithoutOperator[$field->getName()]) ? $filtersWithoutOperator[$field->getName()] : null;

            if ((string) $this->getType($field->getName()) === 'bool' and $value !== null) {
                $value = in_array(strtolower($value), ['true', 1, '1', 'yes', 'tak'], true);
            }

            if ($value !== null) {
                settype($value, (string) $this->getType($field->getName()));
            }

            $this->{$field->getName()} = $value;
        }
    }

    public function getFilters(): FilterContainer {

        $filterContainer = new FilterContainer();
        $reflectionClass = new ReflectionClass(get_class($this));

        $fields = $reflectionClass->getProperties();
       
        foreach ($fields as $field) {

            if ($this->isOrderParam($field)) {

                $filterContainer->createOrder($this->orderColumn,
                        $this->orderDirection);
                continue;
            }
            if (null === $this->{$field->getName()}) {
                continue;
            }

            $name = $field->getName();
            $filterContainer->createFilter($this->getAlias($name) . '.' . $this->getName($name), $this->choseOperator($this->getType($name), $name), $this->{$field->getName()});
        }

        return $filterContainer;
    }

    private function choseOperator(string $type, string $fieldName): string {
        if (isset($this->__operators[$fieldName])) {
            return $this->__operators[$fieldName];
        }

        switch ($type) {
            case 'string':
                return Filter::FILTER_RIGHT_LIKE;
            case 'int':
            case 'bool':
            default:
                return Filter::FILTER_EQ;
        }
    }
    
     private function getName(string $fieldName): string {

        return (isset($this->mapDbName()[$fieldName]) ? $this->mapDbName()[$fieldName]:$fieldName);
    }
    
    private function getType(string $fieldName): string {

        return $this->getParamTypes()[$fieldName];
    }
    
    private function getAlias(string $fieldName): string {

        return $this->getAliasses()[$fieldName];
    }

    private function isOrderParam($field): bool {
     
        if (in_array($field->getName(), ['orderColumn', 'orderDirection']) ) {
            return true;
        }
        return false;
    }

    private function isAvailableOrder($orders): bool {
     
        if (isset($orders['column']) && in_array($orders['column'], $this->getOrderColumns()) && in_array($this->orderDirection, ['asc', 'desc'])
        ) {
            return true;
        }
        
        return false;
    }

}
