<?php

declare(strict_types=1);

namespace App\Application\Cqrs\QueryParams;

use Symfony\Component\HttpFoundation\Request;
use App\Application\Exception\InvalidArgumentException;

class ResponseTypeParams {

    const RESPONSE_TYPE_XLS = 'xls';
    const RESPONSE_TYPE_CSV = 'csv';
    const RESPONSE_TYPE_JSON = 'json';

    protected ?string $responseType;

    function __construct(Request $request) {

        $this->setResponseType($request->query->get('responseType') ?? self::RESPONSE_TYPE_JSON);
    }

    function getResponseType(): ?string {
        return $this->responseType;
    }

    function setResponseType(?string $responseType): void {

        if (!in_array($responseType, [self::RESPONSE_TYPE_XLS, self::RESPONSE_TYPE_CSV, self::RESPONSE_TYPE_JSON])) {
            throw new InvalidArgumentException('Invalid response Type');
        }
        $this->responseType = $responseType;
    }

}
