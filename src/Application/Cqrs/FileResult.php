<?php

declare(strict_types=1);

namespace App\Application\Cqrs;

class FileResult {

    private string $content;
    private string $filename;
    private string $mime;

    public function __construct(string $content, string $filename, string $mime) {
        $this->content = $content;
        $this->filename = $filename;
        $this->mime = $mime;
    }

    /**
     * @return string
     */
    public function getContent(): string {
        return $this->content;
    }

    /**
     * @return string
     */
    public function getFilename(): string {
        return $this->filename;
    }

    /**
     * @return string
     */
    public function getMime(): string {
        return $this->mime;
    }

}
