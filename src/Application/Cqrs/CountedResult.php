<?php

declare(strict_types=1);

namespace App\Application\Cqrs;

class CountedResult {

    private array $items;
    private int $total;

    public function __construct(array $items, int $total) {
        $this->items = $items;
        $this->total = $total;
    }

    /**
     * @return array
     */
    public function getItems(): array {
        return $this->items;
    }

    /**
     * @return int
     */
    public function getTotal(): int {
        return $this->total;
    }

}
