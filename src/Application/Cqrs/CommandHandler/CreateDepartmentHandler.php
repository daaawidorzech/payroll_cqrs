<?php

declare(strict_types=1);

namespace App\Application\Cqrs\CommandHandler;

use App\Infrastructure\Service\PersistenceService;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use App\Application\Cqrs\Command\CreateDepartmentCommand;
use App\Domain\Model\DictDepartment;
use App\Domain\Model\PremiumConfig;
use App\Infrastructure\Doctrine\Type\PremiumTypeEnum;
use App\Application\Exception\InvalidArgumentException;

class CreateDepartmentHandler implements MessageHandlerInterface {

    private PersistenceService $persistenceService;

    function __construct(
            PersistenceService $persistenceService
    ) {
        $this->persistenceService = $persistenceService;
    }

    public function __invoke(CreateDepartmentCommand $command): bool {

        try {
            $this->persistenceService->beginTransaction();
            $department = new DictDepartment();
            $department->setName($command->getName());
            $department->setDescription($command->getDescription());

            $this->persistenceService->persist($department);
            $this->createPremiumConfig($department, $command);

            $this->persistenceService->flush();
            $this->persistenceService->commit();
        } catch (\Throwable $exc) {
         
            $this->persistenceService->rollback();
            throw new InvalidArgumentException("Create Department Error");
        }

        return true;
    }

    private function createPremiumConfig(DictDepartment $department, CreateDepartmentCommand $command): bool {

        $premiumConfig = new PremiumConfig();
        $currency = ($command->getPremiumType() == PremiumTypeEnum::TYPE_CONST_AMOUNT) ? $command->getCurrencyId() : null;
        $premiumConfig->setDictCurrency($currency);
        $premiumConfig->setDictDepartment($department);
        $premiumConfig->setType($command->getPremiumType());
        $premiumConfig->setValue($command->getPremiumValue());

        $this->persistenceService->persist($premiumConfig);

        return true;
    }

}
