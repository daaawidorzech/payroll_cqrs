<?php

declare(strict_types=1);

namespace App\Application\Cqrs\CommandHandler;

use App\Infrastructure\Service\PersistenceService;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use App\Application\Cqrs\Command\CreateEmployeeCommand;
use App\Domain\Model\EmployeeSalary;
use App\Domain\Model\Employee;
use App\Application\Exception\InvalidArgumentException;
use App\Application\Service\Salary\SalaryService;

class CreateEmployeeHandler implements MessageHandlerInterface {

    private PersistenceService $persistenceService;
    private SalaryService $salaryService;

    function __construct(
            PersistenceService $persistenceService,
            SalaryService $salaryService
    ) {
        $this->persistenceService = $persistenceService;
        $this->salaryService = $salaryService;
    }

    public function __invoke(CreateEmployeeCommand $command) {

        try {
            $this->persistenceService->beginTransaction();
            $employee = new Employee();
            $employee->setFirstName($command->getFirstName());
            $employee->setLastName($command->getLastName());
            $employee->setHiredAt($command->getHiredAt());
            $employee->setDictDepartment($command->getDepartmentId());

            $this->persistenceService->persist($employee);

            $this->createSalary($employee, $command);
            $this->persistenceService->commit();
        } catch (\Throwable $exc) {

            $this->persistenceService->rollback();
            throw new InvalidArgumentException("Create Employee Error");
        }

        return true;
    }

    public function createSalary(Employee $employee, CreateEmployeeCommand $command): bool {

        $employeeSalary = new EmployeeSalary();
        $employeeSalary->setEmployee($employee);
        $employeeSalary->setBasicSalary($command->getBasicSalary());
        $employeeSalary->setDictCurrency($command->getCurrencyId());

        $premium = $this->salaryService->getPremium($employeeSalary->getBasicSalary(), $employee->getDictDepartment(), $employee, $employeeSalary->getDictCurrency());
        $employeeSalary->setPremium($premium);
        $employeeSalary->setSalary($this->salaryService->getSalary($employeeSalary->getBasicSalary(), $premium));

        $this->persistenceService->save($employeeSalary);

        return true;
    }

}
