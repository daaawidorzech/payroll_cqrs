<?php

declare(strict_types=1);

namespace App\Application\Cqrs\Query;

use App\Application\Cqrs\QueryParams\EmployeeListParams;
use App\Infrastructure\Doctrine\Repository\EmployeeRepository;

class EmployeeList {

    private EmployeeRepository $employeeRepository;

    public function __construct(
            EmployeeRepository $employeeRepository
    ) {
        $this->employeeRepository = $employeeRepository;
    }

    public function query(EmployeeListParams $listParams): ?array {
        
        return $this->employeeRepository->findByFilters($listParams->getFilters());
    }

}
