<?php

declare(strict_types=1);

namespace App\Application\Cqrs\Command;

use Symfony\Component\Validator\Constraints as Assert;
use App\Domain\Model\DictCurrency;

class CreateDepartmentCommand {

    /**
     *
     * @Assert\NotBlank(message="name is required")
     */
    protected string $name;

    /**
     *
     * @Assert\NotBlank(message="description is required")
     */
    protected string $description;
    protected ?DictCurrency $currencyId;

    /**
     *
     * @Assert\NotBlank(message="premiumType is required")
     */
    protected string $premiumType;

    /**
     *
     * @Assert\NotBlank(message="premiumValue is required")
     */
    protected float $premiumValue;

    function getName(): string {
        return $this->name;
    }

    function getDescription(): string {
        return $this->description;
    }

    function getCurrencyId(): ?DictCurrency {
        return $this->currencyId;
    }

    function getPremiumType(): string {
        return $this->premiumType;
    }

    function getPremiumValue(): float {
        return $this->premiumValue;
    }

    function setName(string $name): void {
        $this->name = $name;
    }

    function setDescription(string $description): void {
        $this->description = $description;
    }

    function setCurrencyId(?DictCurrency $currencyId): void {
        $this->currencyId = $currencyId;
    }

    function setPremiumType(string $premiumType): void {
        $this->premiumType = $premiumType;
    }

    function setPremiumValue(float $premiumValue): void {
        $this->premiumValue = $premiumValue;
    }

}
