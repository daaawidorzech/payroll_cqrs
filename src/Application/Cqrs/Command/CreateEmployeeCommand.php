<?php

declare(strict_types=1);

namespace App\Application\Cqrs\Command;

use Symfony\Component\Validator\Constraints as Assert;
use App\Domain\Model\DictCurrency;
use App\Domain\Model\DictDepartment;

class CreateEmployeeCommand {

    /**
     *
     * @Assert\NotBlank(message="firstName is required")
     */
    protected string $firstName;

    /**
     *
     * @Assert\NotBlank(message="lastName is required")
     */
    protected string $lastName;

    /**
     *
     * @Assert\NotBlank(message="departmentId is required")
     */
    protected DictDepartment $departmentId;

    /**
     *
     * @Assert\NotBlank(message="basicSalary is required")
     */
    protected float $basicSalary;

    /**
     *
     * @Assert\NotBlank(message="hiredAt is required")
     */
    protected \DateTime $hiredAt;

    /**
     *
     * @Assert\NotBlank(message="currencyId is required")
     */
    protected DictCurrency $currencyId;

    function getFirstName(): string {
        return $this->firstName;
    }

    function getLastName(): string {
        return $this->lastName;
    }

    function getDepartmentId(): DictDepartment {
        return $this->departmentId;
    }

    function getBasicSalary(): float {
        return $this->basicSalary;
    }

    function getHiredAt(): \DateTime {
        return $this->hiredAt;
    }

    function getCurrencyId(): DictCurrency {
        return $this->currencyId;
    }

    function setFirstName(string $firstName): void {
        $this->firstName = $firstName;
    }

    function setLastName(string $lastName): void {
        $this->lastName = $lastName;
    }

    function setDepartmentId(DictDepartment $departmentId): void {
        $this->departmentId = $departmentId;
    }

    function setBasicSalary(float $basicSalary): void {
        $this->basicSalary = $basicSalary;
    }

    function setHiredAt(\DateTime $hiredAt): void {
        $this->hiredAt = $hiredAt;
    }

    function setCurrencyId(DictCurrency $currencyId): void {
        $this->currencyId = $currencyId;
    }

}
