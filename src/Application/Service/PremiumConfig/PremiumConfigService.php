<?php

declare(strict_types=1);

namespace App\Application\Service\PremiumConfig;

use App\Infrastructure\Doctrine\Repository\PremiumConfigRepository;
use App\Domain\Model\DictDepartment;
use App\Domain\Model\DictCurrency;
use App\Application\Exception\NotFoundException;
use App\Domain\Model\PremiumConfig;

class PremiumConfigService {

    private PremiumConfigRepository $premiumConfigRepository;

    function __construct(
            PremiumConfigRepository $premiumConfigRepository
    ) {
        $this->premiumConfigRepository = $premiumConfigRepository;
    }

    public function findPremiumConfig(DictDepartment $department): PremiumConfig {

        $config = $this->premiumConfigRepository->findOneBy([
            'dictDepartment' => $department
        ]);

        if (!$config instanceof PremiumConfig) {

            throw new NotFoundException('Premium Config not found');
        }

        return $config;
    }

    public function findConstPremiumConfig(DictDepartment $department, DictCurrency $currency): PremiumConfig {

        $config = $this->premiumConfigRepository->findOneBy([
            'dictDepartment' => $department,
            'dictCurrency' => $currency
        ]);

        if (!$config instanceof PremiumConfig) {

            throw new NotFoundException('Premium Config not found');
        }

        return $config;
    }

}
