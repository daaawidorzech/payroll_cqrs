<?php

declare(strict_types=1);

namespace App\Application\Service\Salary;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use App\Application\Service\PremiumConfig\PremiumConfigService;
use App\Application\Service\Salary\Context\PremiumTypeContext;
use App\Application\Service\Salary\Model\ConstAmountStrategy;
use App\Application\Service\Salary\Model\PercentAmountStrategy;
use App\Domain\Model\DictDepartment;
use App\Domain\Model\DictCurrency;
use App\Domain\Model\Employee;
use App\Application\Exception\InvalidArgumentException;
use App\Infrastructure\Doctrine\Type\PremiumTypeEnum;

class SalaryService {

    private PremiumConfigService $premiumConfigService;
    private ParameterBagInterface $params;
    private string $constAmountYears;

    function __construct(
            PremiumConfigService $premiumConfigService,
            ParameterBagInterface $params
    ) {
        $this->premiumConfigService = $premiumConfigService;
        $this->params = $params;
        $this->constAmountYears = (string) $this->params->get('salary')['constAmountYears'];
    }

    public function getPremium(float $basicSalary, DictDepartment $department, Employee $employee, ?DictCurrency $currency): float {

        $config = $this->premiumConfigService->findPremiumConfig($department);
        $context = new PremiumTypeContext();

        switch ($config->getType()) {
            case PremiumTypeEnum::TYPE_CONST_AMOUNT:
                
                $config = $this->premiumConfigService->findConstPremiumConfig($department, $currency);
                
              
                $context->setPremiumType(new ConstAmountStrategy((string) $config->getValue(), $employee->getHiredAt(), (string) $this->constAmountYears));
                
                break;
            case PremiumTypeEnum::TYPE_PERCENT_AMOUNT:

                $context->setPremiumType(new PercentAmountStrategy((string) $basicSalary, (string) $config->getValue()));

                break;
            default:
                throw new InvalidArgumentException("Invalid premium type");
        }

        return $context->getPremiumType()->getPremiumValue();
    }

    public function getSalary(float $basicSalary, float $premium): float {
        
        return floatval(bcadd((string)$basicSalary, (string)$premium));
    }

}
