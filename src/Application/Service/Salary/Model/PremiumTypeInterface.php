<?php

declare(strict_types=1);

namespace App\Application\Service\Salary\Model;

interface PremiumTypeInterface {

    public function getPremiumValue(): float;
}
