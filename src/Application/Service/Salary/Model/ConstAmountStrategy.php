<?php

declare(strict_types=1);

namespace App\Application\Service\Salary\Model;

use App\Application\Service\Salary\Model\PremiumTypeInterface;

class ConstAmountStrategy implements PremiumTypeInterface {

    private string $premiumPerYear;
    private \DateTimeInterface $hiredAt;
    private string $constAmountYears;

    function __construct(
            string $premiumPerYear,
            \DateTimeInterface $hiredAt,
            string $constAmountYears
    ) {
        $this->premiumPerYear = $premiumPerYear;
        $this->hiredAt = $hiredAt;
        $this->constAmountYears = $constAmountYears;
    }

    public function getPremiumValue(): float {

        $now = new \DateTime();
        $diff = $this->hiredAt->diff($now);

        $diffInYears = (string) $diff->y;

        return floatval(($diffInYears > $this->constAmountYears) ? bcmul($this->premiumPerYear, $this->constAmountYears) : bcmul($this->premiumPerYear, $diffInYears));
    }

}
