<?php

declare(strict_types=1);

namespace App\Application\Service\Salary\Model;

use App\Application\Service\Salary\Model\PremiumTypeInterface;

class PercentAmountStrategy implements PremiumTypeInterface {

    private string $basicSalary;
    private string $percentValue;

    function __construct(
            string $basicSalary,
            string $percentValue
    ) {
        $this->basicSalary = $basicSalary;
        $this->percentValue = $percentValue;
    }

    public function getPremiumValue(): float {

        return floatval(bcmul($this->basicSalary, $this->percentValue));
    }

}
