<?php

declare(strict_types=1);

namespace App\Application\Service\Salary\Context;

use App\Application\Service\Salary\Model\PremiumTypeInterface;

class PremiumTypeContext {

    private PremiumTypeInterface $premiumType;

    function getPremiumType(): PremiumTypeInterface {
        return $this->premiumType;
    }

    function setPremiumType(PremiumTypeInterface $premiumType): void {
        $this->premiumType = $premiumType;
    }

}
