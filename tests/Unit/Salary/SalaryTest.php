<?php

declare(strict_types=1);

namespace App\Tests\Unit\Salary;

use App\Domain\Model\DictCurrency;
use App\Domain\Model\DictDepartment;
use App\Domain\Model\Employee;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SalaryTest extends WebTestCase {

    protected function setUp(): void {

        self::bootKernel();
        $container = self::$kernel->getContainer();
        $container = self::$container;

        $this->premiumService = self::$container->get('App\Application\Service\PremiumConfig\PremiumConfigService');
        $this->salaryService = self::$container->get('App\Application\Service\Salary\SalaryService');
        $this->departmentConstAmount = self::$container->get('doctrine')->getRepository(DictDepartment::class)
                ->findOneBy(['name' => 'hr']);
        $this->departmentPercentAmount = self::$container->get('doctrine')->getRepository(DictDepartment::class)
                ->findOneBy(['name' => 'it']);
        $this->currency = self::$container->get('doctrine')->getRepository(DictCurrency::class)
                ->findOneBy(['name' => 'dollar']);
    }

    public function testCalculatePremiumConstAmountMoreThanTenYears() {

        $basicSalary = 1000;

        $faker = \Faker\Factory::create();

        $body = [
            'firstName' => 'testCalculatePremiumConstAmountMoreThanTenYears',
            'lastName' => $faker->lastName(),
            'departmentId' => $this->departmentConstAmount->getId(),
            'hiredAt' => '1992-01-01',
            'basicSalary' => $basicSalary,
            'currencyId' => $this->currency->getId(),
        ];


        self::ensureKernelShutdown();
        $client = static::createClient();

        $client->request(
                'POST',
                '/employee',
                [],
                [],
                [],
                json_encode($body)
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $employee = self::$container->get('doctrine')->getRepository(Employee::class)->findOneBy(['firstName' => $body['firstName']]);
        $premium = $this->salaryService->getPremium($basicSalary, $this->departmentConstAmount, $employee, $this->currency);

        $this->assertEquals(1000, $premium);

        $salary = $this->salaryService->getSalary($basicSalary, $premium);
        $this->assertEquals(2000, $salary);
    }

    public function testCalculatePremiumConstAmountLessThanTenYears() {

        $basicSalary = floatval(1000);

        $faker = \Faker\Factory::create();

        $body = [
            'firstName' => 'testCalculatePremiumConstAmountLessThanTenYears',
            'lastName' => $faker->lastName(),
            'departmentId' => $this->departmentConstAmount->getId(),
            'hiredAt' => '2016-01-01',
            'basicSalary' => $basicSalary,
            'currencyId' => $this->currency->getId(),
        ];


        self::ensureKernelShutdown();
        $client = static::createClient();

        $client->request(
                'POST',
                '/employee',
                [],
                [],
                [],
                json_encode($body)
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        
        $employee = self::$container->get('doctrine')->getRepository(Employee::class)->findOneBy(['firstName' => $body['firstName']]);
        $premium = $this->salaryService->getPremium($basicSalary, $this->departmentConstAmount, $employee, $this->currency);
        
        $this->assertEquals(500, $premium);
    
        $salary = $this->salaryService->getSalary($basicSalary, $premium);
        $this->assertEquals(1500, $salary);
    }

    public function testCalculatePremiumConstAmountLessThanTenYearsFloatBasicSalary() {

        $basicSalary = 1000.45;

        $faker = \Faker\Factory::create();

        $body = [
            'firstName' => 'testCalculatePremiumConstAmountLessThanTenYearsFloatBasicSalary',
            'lastName' => $faker->lastName(),
            'departmentId' => $this->departmentConstAmount->getId(),
            'hiredAt' => '2016-01-01',
            'basicSalary' => $basicSalary,
            'currencyId' => $this->currency->getId(),
        ];


        self::ensureKernelShutdown();
        $client = static::createClient();

        $client->request(
                'POST',
                '/employee',
                [],
                [],
                [],
                json_encode($body)
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $employee = self::$container->get('doctrine')->getRepository(Employee::class)->findOneBy(['firstName' => $body['firstName']]);
        $premium = $this->salaryService->getPremium($basicSalary, $this->departmentConstAmount, $employee, $this->currency);

        $this->assertEquals(500, $premium);
        $salary = $this->salaryService->getSalary($basicSalary, $premium);
        $this->assertEquals(1500.45, $salary);
    }

    public function testCalculatePremiumPercentAmountLessThanTenYearsFloatBasicSalary() {

        $basicSalary = 1000.45;

        $faker = \Faker\Factory::create();

        $body = [
            'firstName' => 'testCalculatePremiumPercentAmountLessThanTenYearsFloatBasicSalary',
            'lastName' => $faker->lastName(),
            'departmentId' => $this->departmentPercentAmount->getId(),
            'hiredAt' => '2016-01-01',
            'basicSalary' => $basicSalary,
            'currencyId' => $this->currency->getId(),
        ];


        self::ensureKernelShutdown();
        $client = static::createClient();

        $client->request(
                'POST',
                '/employee',
                [],
                [],
                [],
                json_encode($body)
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $employee = self::$container->get('doctrine')->getRepository(Employee::class)->findOneBy(['firstName' => $body['firstName']]);
        $premium = $this->salaryService->getPremium($basicSalary, $this->departmentPercentAmount, $employee, $this->currency);

        $this->assertEquals(150.06, $premium);
        $salary = $this->salaryService->getSalary($basicSalary, $premium);
        $this->assertEquals(1150.51, $salary);
    }

}
