<?php

declare(strict_types=1);

namespace App\Tests\Functional\Employee;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Domain\Model\DictDepartment;
use App\Domain\Model\DictCurrency;

class EmployeeListTest extends WebTestCase {

    private $entityManager;

    protected function setUp(): void {

        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
                ->get('doctrine')
                ->getManager();
    }

    public function testRaportBasicCreate() {

        self::ensureKernelShutdown();
        $client = static::createClient();

        $client->request(
                'GET',
                '/report',
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertResponseHeaderSame('Content-type', 'application/json');
    }

    public function testRaportBasicWithFirstNameCreate() {

        $faker = \Faker\Factory::create();
        $department = $this->entityManager
                ->getRepository(DictDepartment::class)
                ->findOneBy([], ['id' => 'DESC']);

        $currency = $this->entityManager
                ->getRepository(DictCurrency::class)
                ->findOneBy([], ['id' => 'DESC']);

        $firstName = $faker->firstName();
        $body = [
            'firstName' => $firstName,
            'lastName' => $faker->lastName(),
            'departmentId' => $department->getId(),
            'hiredAt' => $faker->dateTimeBetween('-20 years', 'now', 'Europe/Warsaw')->format("Y-m-d"),
            'basicSalary' => $faker->numberBetween(500, 11000),
            'currencyId' => $currency->getId(),
        ];


        self::ensureKernelShutdown();
        $client = static::createClient();

        $client->request(
                'POST',
                '/employee',
                [],
                [],
                [],
                json_encode($body)
        );
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $client->request(
                'GET',
                '/report?filter[firstName]=' . $firstName,
        );
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertStringContainsString($body['firstName'], $client->getResponse()->getContent());
        $this->assertResponseHeaderSame('Content-type', 'application/json');
    }

    public function testRaportBasicWithLastNameCreate() {

        $faker = \Faker\Factory::create();
        $department = $this->entityManager
                ->getRepository(DictDepartment::class)
                ->findOneBy([], ['id' => 'DESC']);

        $currency = $this->entityManager
                ->getRepository(DictCurrency::class)
                ->findOneBy([], ['id' => 'DESC']);


        $body = [
            'firstName' => $faker->firstName(),
            'lastName' => $faker->lastName(),
            'departmentId' => $department->getId(),
            'hiredAt' => $faker->dateTimeBetween('-20 years', 'now', 'Europe/Warsaw')->format("Y-m-d"),
            'basicSalary' => $faker->numberBetween(500, 11000),
            'currencyId' => $currency->getId(),
        ];


        self::ensureKernelShutdown();
        $client = static::createClient();

        $client->request(
                'POST',
                '/employee',
                [],
                [],
                [],
                json_encode($body)
        );
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $client->request(
                'GET',
                '/report?filter[lastName]=' . $body['lastName'],
        );
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertStringContainsString($body['lastName'], $client->getResponse()->getContent());
        $this->assertResponseHeaderSame('Content-type', 'application/json');
    }

    public function testRaportBasicWithDepartmentCreate() {

        $faker = \Faker\Factory::create();
        $department = $this->entityManager
                ->getRepository(DictDepartment::class)
                ->findOneBy([], ['id' => 'DESC']);

        $currency = $this->entityManager
                ->getRepository(DictCurrency::class)
                ->findOneBy([], ['id' => 'DESC']);


        $body = [
            'firstName' => $faker->firstName(),
            'lastName' => $faker->lastName(),
            'departmentId' => $department->getId(),
            'hiredAt' => $faker->dateTimeBetween('-20 years', 'now', 'Europe/Warsaw')->format("Y-m-d"),
            'basicSalary' => $faker->numberBetween(500, 11000),
            'currencyId' => $currency->getId(),
        ];


        self::ensureKernelShutdown();
        $client = static::createClient();

        $client->request(
                'POST',
                '/employee',
                [],
                [],
                [],
                json_encode($body)
        );
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $client->request(
                'GET',
                '/report?filter[departmentName]=' . $department->getDescription(),
        );
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertStringContainsString($department->getDescription(), $client->getResponse()->getContent());
        $this->assertResponseHeaderSame('Content-type', 'application/json');
    }

    public function testRaportXlsxWithFirstNameCreate() {

        $faker = \Faker\Factory::create();
        $department = $this->entityManager
                ->getRepository(DictDepartment::class)
                ->findOneBy([], ['id' => 'DESC']);

        $currency = $this->entityManager
                ->getRepository(DictCurrency::class)
                ->findOneBy([], ['id' => 'DESC']);


        $body = [
            'firstName' => $faker->firstName(),
            'lastName' => $faker->lastName(),
            'departmentId' => $department->getId(),
            'hiredAt' => $faker->dateTimeBetween('-20 years', 'now', 'Europe/Warsaw')->format("Y-m-d"),
            'basicSalary' => $faker->numberBetween(500, 11000),
            'currencyId' => $currency->getId(),
        ];


        self::ensureKernelShutdown();
        $client = static::createClient();

        $client->request(
                'POST',
                '/employee',
                [],
                [],
                [],
                json_encode($body)
        );
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $client->request(
                'GET',
                '/report?responseType=xls&filter[firstName]=' . $body['firstName'],
        );


        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertResponseHeaderSame('Content-type', 'application/vnd.ms-excel');
    }

    public function testRaportCsvWithFirstNameCreate() {

        $faker = \Faker\Factory::create();
        $department = $this->entityManager
                ->getRepository(DictDepartment::class)
                ->findOneBy([], ['id' => 'DESC']);

        $currency = $this->entityManager
                ->getRepository(DictCurrency::class)
                ->findOneBy([], ['id' => 'DESC']);


        $body = [
            'firstName' => $faker->firstName(),
            'lastName' => $faker->lastName(),
            'departmentId' => $department->getId(),
            'hiredAt' => $faker->dateTimeBetween('-20 years', 'now', 'Europe/Warsaw')->format("Y-m-d"),
            'basicSalary' => $faker->numberBetween(500, 11000),
            'currencyId' => $currency->getId(),
        ];


        self::ensureKernelShutdown();
        $client = static::createClient();

        $client->request(
                'POST',
                '/employee',
                [],
                [],
                [],
                json_encode($body)
        );
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $client->request(
                'GET',
                '/report?responseType=csv&filter[firstName]=' . $body['firstName'],
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertResponseHeaderSame('Content-type', 'text/plain');
    }

}
