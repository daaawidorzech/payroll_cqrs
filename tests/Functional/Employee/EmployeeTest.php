<?php

declare(strict_types=1);

namespace App\Tests\Functional\Employee;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Domain\Model\DictDepartment;
use App\Domain\Model\DictCurrency;

class EmployeeTest extends WebTestCase {

    private $entityManager;

    protected function setUp(): void {

        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
                ->get('doctrine')
                ->getManager();
    }

    public function testEmployeeCreate() {

        $faker = \Faker\Factory::create();
        $department = $this->entityManager
                ->getRepository(DictDepartment::class)
                ->findOneBy([], ['id' => 'DESC']);
        ;


        $currency = $this->entityManager
                ->getRepository(DictCurrency::class)
                ->findOneBy([], ['id' => 'DESC']);
        ;


        $body = [
            'firstName' => $faker->firstName(),
            'lastName' => $faker->lastName(),
            'departmentId' => $department->getId(),
            'hiredAt' => $faker->dateTimeBetween('-20 years', 'now', 'Europe/Warsaw')->format("Y-m-d"),
            'basicSalary' => $faker->numberBetween(500, 11000),
            'currencyId' => $currency->getId(),
        ];


        self::ensureKernelShutdown();
        $client = static::createClient();

        $client->request(
                'POST',
                '/employee',
                [],
                [],
                [],
                json_encode($body)
        );
      
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertJsonStringEqualsJsonString(
                '{"success": true}',
                $client->getResponse()->getContent()
        );
        $this->assertResponseHeaderSame('Content-type', 'application/json');
    }

    public function testEmployeeCreateFloatSalary() {

        $faker = \Faker\Factory::create();
        $department = $this->entityManager
                ->getRepository(DictDepartment::class)
                ->findOneBy([], ['id' => 'DESC']);
        ;


        $currency = $this->entityManager
                ->getRepository(DictCurrency::class)
                ->findOneBy([], ['id' => 'DESC']);
        ;


        $body = [
            'firstName' => $faker->firstName(),
            'lastName' => $faker->lastName(),
            'departmentId' => $department->getId(),
            'hiredAt' => $faker->dateTimeBetween('-20 years', 'now', 'Europe/Warsaw')->format("Y-m-d"),
            'basicSalary' => 11000.45,
            'currencyId' => $currency->getId(),
        ];


        self::ensureKernelShutdown();
        $client = static::createClient();

        $client->request(
                'POST',
                '/employee',
                [],
                [],
                [],
                json_encode($body)
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertJsonStringEqualsJsonString(
                '{"success": true}',
                $client->getResponse()->getContent()
        );
        $this->assertResponseHeaderSame('Content-type', 'application/json');
    }

    public function testEmployeeCreateMissingField() {

        $faker = \Faker\Factory::create();
        $department = $this->entityManager
                ->getRepository(DictDepartment::class)
                ->findOneBy([], ['id' => 'DESC']);
        ;


        $currency = $this->entityManager
                ->getRepository(DictCurrency::class)
                ->findOneBy([], ['id' => 'DESC']);
        ;


        $body = [
            'firstName' => $faker->firstName(),
            'lastName' => $faker->lastName(),
            'departmentId' => $department->getId(),
            'hiredAt' => $faker->dateTimeBetween('-20 years', 'now', 'Europe/Warsaw')->format("Y-m-d"),
            'currencyId' => $currency->getId(),
        ];


        self::ensureKernelShutdown();
        $client = static::createClient();

        $client->request(
                'POST',
                '/employee',
                [],
                [],
                [],
                json_encode($body)
        );

        $this->assertEquals(400, $client->getResponse()->getStatusCode());
        $this->assertJsonStringEqualsJsonString(
                '{"error":{"errorId":400,"errorText":"basicSalary is required. ","errorArray":{"basicSalary":["basicSalary is required"]}}}',
                $client->getResponse()->getContent()
        );
        $this->assertResponseHeaderSame('Content-type', 'application/json');
    }

}
