<?php

declare(strict_types=1);

namespace App\Tests\Functional\Employee;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Domain\Model\DictCurrency;

class DepartmentTest extends WebTestCase {

    private $entityManager;

    protected function setUp(): void {

        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
                ->get('doctrine')
                ->getManager();
    }

    public function testDepartmentConstAmountCreate() {

        $faker = \Faker\Factory::create();

        $currency = $this->entityManager
                ->getRepository(DictCurrency::class)
                ->findOneBy([], ['id' => 'DESC']);

        $body = [
            'name' => md5("CONST_AMOUNT" . date("Y-m-d h:i:s")),
            'description' => md5("desciption" . date("Y-m-d h:i:s")),
            'premiumType' => "CONST_AMOUNT",
            'premiumValue' => $faker->numberBetween(500, 11000),
            'currencyId' => $currency->getId(),
        ];

        self::ensureKernelShutdown();
        $client = static::createClient();

        $client->request(
                'POST',
                '/department',
                [],
                [],
                [],
                json_encode($body)
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertJsonStringEqualsJsonString(
                '{"success": true}',
                $client->getResponse()->getContent()
        );
        $this->assertResponseHeaderSame('Content-type', 'application/json');
    }

    public function testDepartmentPercentAmountCreate() {

        $faker = \Faker\Factory::create();

        $currency = $this->entityManager
                ->getRepository(DictCurrency::class)
                ->findOneBy([], ['id' => 'DESC']);

        $body = [
            'name' => md5("PERCENT_AMOUNT" . date("Y-m-d h:i:s")),
            'description' => "desciption" . md5(date("Y-m-d h:i:s")),
            'premiumType' => "PERCENT_AMOUNT",
            'premiumValue' => $faker->numberBetween(500, 11000),
            'currencyId' => $currency->getId(),
        ];


        self::ensureKernelShutdown();
        $client = static::createClient();

        $client->request(
                'POST',
                '/department',
                [],
                [],
                [],
                json_encode($body)
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertJsonStringEqualsJsonString(
                '{"success": true}',
                $client->getResponse()->getContent()
        );
        $this->assertResponseHeaderSame('Content-type', 'application/json');
    }

    public function testDepartmentPercentAmountMissingFields() {

        $faker = \Faker\Factory::create();

        $body = [
            'name' => "name" . md5(date("Y-m-d h:i:s")),
            'description' => "desciption" . md5(date("Y-m-d h:i:s")),
            'premiumValue' => $faker->numberBetween(500, 11000),
        ];

        self::ensureKernelShutdown();
        $client = static::createClient();

        $client->request(
                'POST',
                '/department',
                [],
                [],
                [],
                json_encode($body)
        );

        $this->assertEquals(400, $client->getResponse()->getStatusCode());
        $this->assertJsonStringEqualsJsonString(
                '{"error":{"errorId":400,"errorText":"Create Department Error","errorArray":[]}}',
                $client->getResponse()->getContent()
        );
        $this->assertResponseHeaderSame('Content-type', 'application/json');
    }

    public function testDepartmentPercentAmountCreateDuplicate() {

        $faker = \Faker\Factory::create();

        $currency = $this->entityManager
                ->getRepository(DictCurrency::class)
                ->findOneBy([], ['id' => 'DESC']);

        $name = md5("PERCENT_AMOUNT" . date("Y-m-d h:i:s"));
        $body = [
            'name' => $name,
            'description' => "desciption" . md5(date("Y-m-d h:i:s")),
            'premiumType' => "PERCENT_AMOUNT",
            'premiumValue' => $faker->numberBetween(500, 11000),
            'currencyId' => $currency->getId(),
        ];

        self::ensureKernelShutdown();
        $client = static::createClient();

        $client->request(
                'POST',
                '/department',
                [],
                [],
                [],
                json_encode($body)
        );

        self::ensureKernelShutdown();
        $client = static::createClient();

        $client->request(
                'POST',
                '/department',
                [],
                [],
                [],
                json_encode($body)
        );


        $this->assertEquals(400, $client->getResponse()->getStatusCode());
        $this->assertJsonStringEqualsJsonString(
                '{"error":{"errorId":400,"errorText":"Create Department Error","errorArray":[]}}',
                $client->getResponse()->getContent()
        );
        $this->assertResponseHeaderSame('Content-type', 'application/json');
    }

}
