# payroll_tidio

**Uruchomienie aplikacji w docker:**

1.Uruchamiamy w katalogu głównym aplikacji docker-compose up -d --build

2.Wchodzimy w kontener z php fpm docker: docker exec -it [id lub nazwa utworzonego kontenera php fpm] /bin/bash - nazwę lub id pobieramy z polecenia: docker ps

3.W kontenerze php fpm uruchamiamy po kolei:
- 	- composer install
- 	- chmod -R 777 vendor
- 	- **migracje:** php bin/console doctrine:migrations:migrate
- 	- **migracje-test do testów unit:** php bin/console --env=test doctrine:migrations:migrate
- 	- **utworzenie słowników:** php bin/console app:create-dict-fixture
- 	- **utworzenie słowników do testów unit:** php bin/console --env=test app:create-dict-fixture
- 	- **utworzenie danych ( fixtures):** php bin/console app:create-employee-fixture
- 	- **utworzenie danych do testów unit (fixtures):** php bin/console --env=test app:create-employee-fixture

	
---------------------------------------------------------------------------------------------------
	
**Pobieranie raportu:**

http://localhost:53000/report?responseType=xls

resposneType=xls/csv/json - typ zwrotu danych (plik excel/csv/json) (domyślnie json)


**Możliwe filtry:**
filter[firstName/lastName/departmentName]

Przykład wszystkich filtrów
http://localhost:53000/report?responseType=xls&filter[firstName]=$imie&filter[lastName]=$nazwisko&filter[departmentName]=$nazwa_działu

Jest możliwość filtrowania konkretnych kolumn:

Przykłady:
http://localhost:53000/report?responseType=xls&filter[firstName]=$imie
http://localhost:53000/report?responseType=xls&filter[firstName]=$imie&filter[lastName]=$nazwisko
http://localhost:53000/report?responseType=xls&filter[departmentName]=$nazwa_działu



**Sortowanie:**

Sortowanie wymaga podania kolumny i/lub kierunku:

- order[column]=firstName/lastName/departmentName/basicSalary/premium/premiumType/salary - kolumna, po której sortujemy
- order[direction]=asc/desc - kierunek sortowania


http://localhost:53000/report?responseType=xls&order[column]=$name&order[direction]=$kierunek_sortowania

Jest możliwość sortowania i filtrowania jednocześnie:

http://localhost:53000/report?responseType=xls&order[column]=$name&order[direction]=$kierunek_sortowania&filter[lastName]=$nazwisko


---------------------------------------------------------------------------------------------------





**Dodawanie działów:**



http://localhost:53000/department [POST]

JsonBody:

{
	"name":"hr",
	"description":"HR",
	"currencyId":1,
	"premiumType":"CONST_AMOUNT",
	"premiumValue":100
}


- name=nazwa działu
- description=opis działu
- currencyId = id waluty wypłaty w dziale
- premiumType=CONST_AMOUNT/PERCENT_AMOUNT Kwota stała doliczana do pensji podstawowej, za każdy rok pracy / Procentowa
- premiumValue=10 = wartość prowizji 

    -w przypadku CONST_AMOUNT podajemy stałą wartość kwoty np. 1000 dla doliczenia za każdy rok

    -w przypadku PERCENT_AMOUNT podajemy wartość np. 0.15 dla 15%, 0.45 dla 45% , 1 dla 100%


**Dodawanie pracowników:**

http://localhost:53000/employee [POST]

JsonBody:

{
	"firstName":"Dawid",
	"lastName": "Orzechowski",
	"departmentId":1,
	"hiredAt": "2015-01-01",
	"basicSalary":1000,
	"currencyId":1
	
}

- firstName=imię
- lastName=nazwisko
- departmentId=id działu
- hiredAt=data zatrudnienia pracownika
- basicSalary=podstawa wynagrodzenia
- currencyId=id waluty wynagrodzenia


**Testy**

Wchodzimy w kontener z php fpm docker: docker exec -it [id lub nazwa utworzonego kontenera php fpm] /bin/bash i 
uruchamiamy **php bin/phpunit**


